# 📱 Projeto Automação Mobile

Bem-vindo ao **Projeto Automação Mobile**! Este projeto utiliza **Appium**, **Cucumber**, **JUnit** e **Log4j** para testar aplicações móveis de forma automatizada.

## 🛠️ Tecnologias Utilizadas

- **Appium**: Versão 9.2.3
- **Cucumber**: Versão 7.11.0
- **JUnit**: Versão 4.13.2
- **Log4j**: Versão 2.20.0

## 🚀 Começando

### Pré-requisitos

Para executar este projeto, você precisará ter o seguinte instalado:

- **[Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)**
- **[Maven](https://maven.apache.org/download.cgi)**
- **[Appium](http://appium.io/)**

### Instalação

1. Clone o repositório:
   ```sh
   git clone https://github.com/seu-usuario/projeto_automain_mobile.git

2. Instalar Dependências do Maven:
   ```sh
   cd projeto_automain_mobile

3. Instale as dependências do Maven:
   ```sh
   mvn clean install

4. Executar os Testes
   ```sh
    mvn test

## 🤝 Contribuição

**Contribuições são bem-vindas! Sinta-se à vontade para abrir issues e pull requests no repositório do projeto.**