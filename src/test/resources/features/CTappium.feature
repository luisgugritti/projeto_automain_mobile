@CTappium
  Feature: Aplicativo CTappium

    Background: Realizar Testes no Aplicativo CTappium
      Given acessar o aplicativo CTappium

      @CT001
      Scenario: Entrar no Menu About
        When clicar no menu About
        Then deve visualizar as informacoes do menu About

      @CT002
      Scenario: Entrar no Menu Formulário e Escolher PS4
        When clicar no menu Formulario
        And escolher o nome PS4
        And rolar o ponto na linha para diminuir
        And selecionar Horario
        And selecionar Data
        And clicar no botao Salvar
        Then validar nome PS4 salvo

      @CT003
      Scenario: Entrar no Menu Formulário e Escolher Nintendo Switch
        When clicar no menu Formulario
        And escolher o nome Nintendo Switch
        And rolar o ponto na linha para aumentar
        And selecionar Horario
        And selecionar Data
        And clicar no botao Salvar
        Then validar nome Nintendo Switch salvo

      @CT004
      Scenario: Entrar no Menu Formulário e Escolher Xbox
        When clicar no menu Formulario
        And rolar o ponto na linha para diminuir
        And selecionar Horario
        And selecionar Data
        And clicar no botao Salvar
        Then validar nome Xbox salvo

      @CT005
      Scenario: Validar botao Salvar Demorado
        When clicar no menu Formulario
        And rolar o ponto na linha para diminuir
        And selecionar Horario
        And selecionar Data
        And clicar no botao Salvar Demorado
        Then validar nome Xbox salvo

      @CT006
      Scenario: Validar botao Limpar
        When clicar no menu Formulario
        And rolar o ponto na linha para diminuir
        And selecionar Horario
        And selecionar Data
        And clicar no botao Salvar
        And clicar no botao Limpar
        Then validar que as informacoes salvas foram apagadas