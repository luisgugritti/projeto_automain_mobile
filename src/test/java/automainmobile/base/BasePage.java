package automainmobile.base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class BasePage {

    protected static AppiumDriver driver;
    protected WebDriverWait wait;

    public BasePage(AppiumDriver driver) {
        BasePage.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void escrever(WebElement write, String escrever) {
        if (write != null) {
            write.sendKeys(escrever);
        } else {
            throw new IllegalArgumentException("WebElement for writing is null");
        }
    }

    public void escreverEntrar(WebElement write, String escrever) {
        if (write != null) {
            write.sendKeys(escrever, Keys.ENTER);
        } else {
            throw new IllegalArgumentException("WebElement for writing is null");
        }
    }

    public void clicarBotao(WebElement btn) {
        if (btn != null) {
            btn.click();
        } else {
            throw new IllegalArgumentException("WebElement for click is null");
        }
    }

    public void esperarElemento(WebElement elemento) {
        try {
            wait.until(ExpectedConditions.visibilityOf(elemento));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void limparCampo(WebElement campo) {
        if (campo != null) {
            campo.clear();
        } else {
            throw new IllegalArgumentException("WebElement for clearing is null");
        }
    }

    public void rolarAteElemento(WebElement elemento) {
        if (elemento != null) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemento);
        } else {
            throw new IllegalArgumentException("WebElement for scrolling is null");
        }
    }

    public void selecionarPorTexto(WebElement dropdown, String texto) {
        if (dropdown != null) {
            Select select = new Select(dropdown);
            select.selectByVisibleText(texto);
        } else {
            throw new IllegalArgumentException("WebElement for selecting dropdown is null");
        }
    }

    public void aceitarAlerta() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }

    public void rejeitarAlerta() {
        try {
            driver.switchTo().alert().dismiss();
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }

    public void moverParaElemento(WebElement elemento) {
        if (elemento != null) {
            Actions actions = new Actions(driver);
            actions.moveToElement(elemento).perform();
        } else {
            throw new IllegalArgumentException("WebElement for moving to is null");
        }
    }

    public void selecionarPorValor(WebElement dropdown, String valor) {
        if (dropdown != null) {
            Select select = new Select(dropdown);
            select.selectByValue(valor);
        } else {
            throw new IllegalArgumentException("WebElement for selecting dropdown is null");
        }
    }

    public void pausar(int segundos) {
        try {
            Thread.sleep(segundos * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean verificarElementoVisivel(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean verificarElementoInvisivel(WebElement element) {
        try {
            return !element.isDisplayed();
        } catch (NoSuchElementException e) {
            return true;
        }
    }

    public void rolarElemento(WebElement element, int distanceX, int distanceY) {
        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);

        touchAction.press(ElementOption.element(element))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(ElementOption.element(element, distanceX, distanceY))
                .release()
                .perform();
    }

}
