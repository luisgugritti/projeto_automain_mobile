package automainmobile.base;

public class VariaveisGlobais {

    // Variáveis Aplicativo CTappium
    public static final String txtMenuAbout = "Testes funcionais de aplicações Android com Appium";
    public static final String txtMenuPs4 = "PS4";
    public static final String txtMenuNintendo = "Nintendo Switch";
    public static final String txtMenuXbox = "XBox One";

}
