package automainmobile.ctappium.steps;

import automainmobile.ctappium.page.CTappiumPage;
import automainmobile.utils.Hooks;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CTappiumSteps {

    private CTappiumPage ctAppiumPage;

    @Given("acessar o aplicativo CTappium")
    public void entrarNoAplicativoCTAppium() {
        ctAppiumPage = new CTappiumPage(Hooks.getDriver());
    }

    @When("^clicar no menu (.*)$")
    public void clicarNoMenuAbout(String menu) {
        ctAppiumPage.clicarNoMenuAbout(menu);
    }

    @Then("deve visualizar as informacoes do menu About")
    public void deveVisualizarAsInformacoesDoMenuAbout() {
        ctAppiumPage.deveVisualizarAsInformacoesDoMenuAbout();
    }

    @And("^escolher o nome (.*)$")
    public void escolherNome(String nome) throws InterruptedException {
        ctAppiumPage.escolherNome(nome);
    }

    @And("^selecionar (.*)$")
    public void selecionar(String tipo) throws InterruptedException {
        ctAppiumPage.selecionar(tipo);
    }

    @And("^clicar no botao (.*)$")
    public void clicarNoBotaoSalvar(String tipo) {
        ctAppiumPage.clicarNoBotaoSalvar(tipo);
    }

    @Then("^validar nome (.*) salvo$")
    public void validarNomeSalvo(String nome) {
        ctAppiumPage.validarNomeSalvo(nome);
    }

    @Then("validar que as informacoes salvas foram apagadas")
    public void validarQueAsInformacoesSalvasForamApagadas() {
        ctAppiumPage.validarQueAsInformacoesSalvasForamApagadas();
    }

    @And("^rolar o ponto na linha para (.*)$")
    public void rolarOPontoNaLinhaPara(String ponto) {
        ctAppiumPage.rolarOPontoNaLinhaPara(ponto);
    }

}
