package automainmobile.ctappium.page;

import automainmobile.base.BasePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import static automainmobile.base.VariaveisGlobais.*;
import static java.lang.Thread.sleep;

public class CTappiumPage extends BasePage {

    private final CTappiumElements ctAppiumElements;

    public CTappiumPage(AppiumDriver driver) {
        super(driver);
        ctAppiumElements = new CTappiumElements();
        PageFactory.initElements(new AppiumFieldDecorator(driver), ctAppiumElements);
    }

    public void clicarNoMenuAbout(String menu) {
        switch (menu) {
            case "About":
                esperarElemento(ctAppiumElements.selectMenuAbout);
                clicarBotao(ctAppiumElements.selectMenuAbout);
                break;
            case "Formulario":
                esperarElemento(ctAppiumElements.selectMenuFormulario);
                clicarBotao(ctAppiumElements.selectMenuFormulario);
                break;
            default:
                throw new IllegalArgumentException("Menu não reconhecido: " + menu);
        }
    }

    public void escolherNome(String nome) throws InterruptedException {
        sleep(1000);
        clicarBotao(ctAppiumElements.escolherNome);

        switch (nome) {
            case "PS4":
                esperarElemento(ctAppiumElements.ps4);
                clicarBotao(ctAppiumElements.ps4);
                break;
            case "Nintendo Switch":
                esperarElemento(ctAppiumElements.nintendoSwitch);
                clicarBotao(ctAppiumElements.nintendoSwitch);
                break;
            default:
                throw new IllegalArgumentException("Nome não reconhecido: " + nome);
        }
    }

    public void selecionar(String tipo) throws InterruptedException {
        switch (tipo) {
            case "Horario":
                sleep(1000);
                clicarBotao(ctAppiumElements.selectHora);
                clicarBotao(ctAppiumElements.selectHora);
                break;
            case "Data":
                sleep(1000);
                clicarBotao(ctAppiumElements.selectDta);
                break;
            default:
                throw new IllegalArgumentException("Seleção " + tipo +" não reconhecida.");
        }
    }

    public void clicarNoBotaoSalvar(String tipo) {
        switch (tipo) {
            case "Limpar":
                clicarBotao(ctAppiumElements.btnLimpar);
                break;
            case "Salvar":
                clicarBotao(ctAppiumElements.btnSave);
                break;
            case "Salvar Demorado":
                clicarBotao(ctAppiumElements.btnSalvarDemorado);
                break;
            default:
                throw new IllegalArgumentException("Botão " + tipo +" não reconhecido.");
        }
    }

    public void deveVisualizarAsInformacoesDoMenuAbout() {
        Assert.assertTrue("Validar informações do Menu About",
                ctAppiumElements.menuAbout.getText().contains(txtMenuAbout));
    }

    public void validarNomeSalvo(String nome) {
        switch (nome) {
            case "PS4":
                esperarElemento(ctAppiumElements.textViewFormulario);

                Assert.assertEquals("Validando mensagem sobre nome salvo",
                        ctAppiumElements.textViewFormulario.getText(), txtMenuPs4);
                break;
            case "Nintendo Switch":
                esperarElemento(ctAppiumElements.textViewFormulario);

                Assert.assertEquals("Validando mensagem sobre nome salvo",
                        ctAppiumElements.textViewFormulario.getText(), txtMenuNintendo);
                break;
            case "Xbox":
                esperarElemento(ctAppiumElements.textViewFormulario);

                Assert.assertEquals("Validando mensagem sobre nome salvo",
                        ctAppiumElements.textViewFormulario.getText(), txtMenuXbox);
                break;
            default:
                throw new IllegalArgumentException("Nome " + nome +" não reconhecido.");
        }
    }

    public void validarQueAsInformacoesSalvasForamApagadas() {
        Assert.assertFalse("Validar deletação de informações salvas após clicar em Limpar",
                verificarElementoInvisivel(ctAppiumElements.textViewFormulario));
    }

    public void rolarOPontoNaLinhaPara(String ponto) {
        switch (ponto) {
            case "aumentar":
                rolarElemento(ctAppiumElements.rolarPonto, 200, 50);
                break;
            case "diminuir":
                rolarElemento(ctAppiumElements.rolarPonto, 0, 50);
                break;
            default:
                throw new IllegalArgumentException("O " + ponto + " não foi reconhecido.");
        }
    }

}
