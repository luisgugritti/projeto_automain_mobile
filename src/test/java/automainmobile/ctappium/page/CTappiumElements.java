package automainmobile.ctappium.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CTappiumElements {

    @FindBy(className = "android.widget.TextView")
    public WebElement selectMenuAbout;

    @FindBy(xpath = "//android.widget.TextView[@text='Formulário']")
    public WebElement selectMenuFormulario;

    @FindBy(xpath = "//*[contains(@text, 'Testes funcionais de aplicações Android com Appium')]")
    public WebElement menuAbout;

    @FindBy(xpath = "//android.widget.Spinner[@content-desc='console']")
    public WebElement escolherNome;

    @FindBy(xpath = "//android.widget.CheckedTextView[@text='PS4']")
    public WebElement ps4;

    @FindBy(xpath = "//android.widget.CheckedTextView[@text='Nintendo Switch']")
    public WebElement nintendoSwitch;

    @FindBy(xpath = "//android.widget.CheckBox[@content-desc='check']")
    public WebElement selectDta;

    @FindBy(xpath = "//android.widget.Button[@content-desc='save']")
    public WebElement btnSave;

    @FindBy(xpath = "//android.widget.TextView[@text='SALVAR DEMORADO']")
    public WebElement btnSalvarDemorado;

    @FindBy(xpath = "//android.widget.Switch[@content-desc='switch']")
    public WebElement selectHora;

    @FindBy(xpath = "//android.widget.TextView[@text='LIMPAR']")
    public WebElement btnLimpar;

    @FindBy(xpath = "(//android.widget.TextView)[2]")
    public WebElement textViewFormulario;

    @FindBy(xpath = "//android.view.ViewGroup[@content-desc='slid']/android.view.ViewGroup[2]")
    public WebElement rolarPonto;

}
