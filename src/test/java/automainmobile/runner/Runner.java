package automainmobile.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = false,
        stepNotifications = false,
        useFileNameCompatibleName = false,
        features = "src/test/resources/features",
        glue = "automainmobile",
        plugin = {
                "pretty",
                "junit:target/surefire-reports/result.xml",
                "html:target/cucumber-reports"
        },
        tags = "@CTappium and @CT003"
)
public class Runner {
}
