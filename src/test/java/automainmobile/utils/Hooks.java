package automainmobile.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.options.XCUITestOptions;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class Hooks {

    private static AppiumDriver driver;
    private static final Properties properties = new Properties();
    private static String currentScenarioId;

    static {
        try (InputStream input = Hooks.class.getClassLoader().getResourceAsStream("config/config.properties")) {
            if (input == null) {
                System.out.println("Desculpe, não foi possível encontrar config.properties");
            } else {
                properties.load(input);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Before
    public void setUp(Scenario scenario) throws Exception {
        currentScenarioId = scenario.getId();
        String platform = properties.getProperty("platform");

        if (platform == null) {
            throw new IllegalArgumentException("Platform não pode ser nula.");
        }

        if (platform.equalsIgnoreCase("Android")) {
            startAppiumAndroid();
        } else if (platform.equalsIgnoreCase("iOS")) {
            startAppiumIphone();
        } else {
            throw new IllegalArgumentException("Plataforma desconhecida: " + platform);
        }
    }

    @AfterStep
    public void embedScreenshot(Scenario scenario) {
        if (currentScenarioId.equals(scenario.getId())) {
            Screenshots.captureAndSaveScreenshot(driver, scenario.getName());
            embedScreenshotDescription(scenario);
        }
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    private void embedScreenshotDescription(Scenario scenario) {
        String screenshotDescription = "Screenshot captured for step: " + scenario.getName();
        scenario.attach(screenshotDescription.getBytes(), "text/plain", scenario.getName());
    }

    private static void startAppiumAndroid() {
        try {
            UiAutomator2Options options = new UiAutomator2Options()
                    .setDeviceName(properties.getProperty("android.deviceName"))
                    .setPlatformName(properties.getProperty("android.platformName"))
                    .setAppPackage(properties.getProperty("android.appPackage"))
                    .setAppActivity(properties.getProperty("android.appActivity"));

            URL serverUrl = new URL(properties.getProperty("url.remote"));
            driver = new AndroidDriver(serverUrl, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void startAppiumIphone() {
        try {
            XCUITestOptions options = new XCUITestOptions()
                    .setDeviceName(properties.getProperty("ios.deviceName"))
                    .setPlatformName(properties.getProperty("ios.platformName"))
                    .setBundleId(properties.getProperty("ios.bundleId"));

            URL serverUrl = new URL(properties.getProperty("url.remote"));
            driver = new IOSDriver(serverUrl, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static AppiumDriver getDriver() {
        return driver;
    }

}
