package automainmobile.utils;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Screenshots {

    private static final String screenshotDirectory = "target/screenshots";

    public static void captureAndSaveScreenshot(AppiumDriver driver, String screenshotName) {
        createScreenshotDirectory();
        String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS"));
        String screenshotPath = screenshotDirectory + File.separator + screenshotName + "_" + timestamp + ".png";

        try {
            File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            Files.copy(screenshotFile.toPath(), Paths.get(screenshotPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createScreenshotDirectory() {
        try {
            Files.createDirectories(Paths.get(screenshotDirectory));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
